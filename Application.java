class Application{
	public static void main(String[] args){
		Student someone = new Student();
		someone.name = "Someone you don't know";
		someone.socialInsuranceNum = 11223344;
		someone.passingOrNo = true;
		
		Student someoneElse = new Student();
		someoneElse.name = "Another person that you don't know";
		someoneElse.socialInsuranceNum = 33445566;
		someoneElse.passingOrNo = false;
		
		Student[] section4 = new Student[3];
		section4[0] = someone;
		section4[1] = someoneElse;
		section4[2] = new Student();
		section4[2].name = "Another person";
		section4[2].socialInsuranceNum = 66778899;
		section4[2].passingOrNo = true;
		
		/*System.out.println(someone.name);
		System.out.println(someone.socialInsuranceNum);
		System.out.println(someone.passingOrNo);
		
		System.out.println(someoneElse.name);
		System.out.println(someoneElse.socialInsuranceNum);
		System.out.println(someoneElse.passingOrNo);
		
		System.out.println(someone.fullNameMethod("meow meow"));
		System.out.println(someoneElse.displayPassingOrNot());
		
		System.out.println(section4[0].name);*/
		System.out.println(section4[2].name);
		
	}
}